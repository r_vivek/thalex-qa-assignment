import HomePage from "../support/pageObjects/HomePage";
import ApiHandler from "../support/api/ApiHandler";
import ProfilePage from "../support/pageObjects/ProfilePage";

describe("Real World App - ProfilePage test scenarios", () => {
  const homePage = new HomePage();
  const profilePage = new ProfilePage();

  beforeEach(() => {
    ApiHandler.initInterceptors();
  });

  it("Clicking on a username loads the user's page", () => {
    homePage.visitHomePage();
    homePage.selectRandomUser();
    profilePage.isProfilePageUrlCorrect();
    profilePage.areArticleAuthorsCorrect();
    profilePage.isProfileHeaderCorrect();
  });
});
