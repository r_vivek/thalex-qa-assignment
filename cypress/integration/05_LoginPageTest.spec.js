import LoginPage from "../support/pageObjects/LoginPage";
import MenuNavigation from "../support/pageObjects/MenuNavigation";
import HomePage from "../support/pageObjects/HomePage";
import ApiHandler from "../support/api/ApiHandler";

describe("Real World App - Login Page test scenarios", () => {
  let users;
  const homePage = new HomePage();
  const loginPage = new LoginPage();
  const menuNavigation = new MenuNavigation();
  beforeEach(() => {
    ApiHandler.initInterceptors();
    cy.fixture("UserCredentials").then((userCredentials) => {
      users = userCredentials;
    });
  });
  it("Check whether login button is disabled when the fields are empty", () => {
    homePage.visitHomePage();
    menuNavigation.visitLoginPage();
    loginPage.isLoginButtonDisabled();
  });
  it("Check whether login button is disabled when the email is filled but password is empty", () => {
    homePage.visitHomePage();
    menuNavigation.visitLoginPage();
    loginPage.fillEmail("test@example.com");
    loginPage.isLoginButtonDisabled();
  });
  it("Check whether login button is disabled when the password is filled but email is empty", () => {
    homePage.visitHomePage();
    menuNavigation.visitLoginPage();
    loginPage.fillPassword("test");
    loginPage.isLoginButtonDisabled();
  });
  it("Check whether password field has hidden characters", () => {
    homePage.visitHomePage();
    menuNavigation.visitLoginPage();
    loginPage.fillPassword("test");
    loginPage.isPasswordHidden();
  });
  it("User able to login with correct credentials", () => {
    homePage.visitHomePage();
    menuNavigation.visitLoginPage();
    loginPage.isLoginPageUrlCorrect();
    loginPage.loginUser(users.correctUser);
    loginPage.isLoginSuccessful();
    loginPage.isUserNameDisplayedOnTheTopRight();
  });
  it("User should not able to login with incorrect credentials", () => {
    homePage.visitHomePage();
    menuNavigation.visitLoginPage();
    loginPage.isLoginPageUrlCorrect();
    loginPage.loginUser(users.incorrectUser);
    loginPage.isLoginUnauthorized();
    loginPage.isUserNameAbsentOnTheTopRight();
  });
  it("Error messages shown in case of unauthorized login", () => {
    homePage.visitHomePage();
    menuNavigation.visitLoginPage();
    loginPage.isLoginPageUrlCorrect();
    loginPage.loginUser(users.incorrectUser);
    loginPage.areErrorMessagesShown();
  });
});
