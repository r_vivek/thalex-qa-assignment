import HomePage from "../support/pageObjects/HomePage";
import ArticlePage from "../support/pageObjects/ArticlePage";
import ApiHandler from "../support/api/ApiHandler";

describe("Real World App - ArticlePage test scenarios", () => {
  const homePage = new HomePage();
  const articlePage = new ArticlePage();

  beforeEach(() => {
    ApiHandler.initInterceptors();
  });

  it("Clicking on an article loads it in a new page", () => {
    homePage.visitHomePage();
    homePage.selectRandomArticle();
    articlePage.IsArticlePageUrlCorrect();
    articlePage.IsArticleHeaderCorrect();
  });
});
