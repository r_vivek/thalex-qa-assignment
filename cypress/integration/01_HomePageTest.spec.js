import HomePage from "../support/pageObjects/HomePage";
import ApiHandler from "../support/api/ApiHandler";

describe("Real World App - HomePage test scenarios", () => {
  const homePage = new HomePage();

  beforeEach(() => {
    ApiHandler.initInterceptors();
  });

  it("Viewing the landing page works, and checking that a list of articles load", () => {
    homePage.visitHomePage();
    homePage.isHomePageUrlCorrect();
    homePage.areArticlesPresent();
  });
});
