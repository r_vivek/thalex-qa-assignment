import HomePage from "../support/pageObjects/HomePage";
import ApiHandler from "../support/api/ApiHandler";
import TagPage from "../support/pageObjects/TagPage";

describe("Real World App - TagPage test scenarios", () => {
  const homePage = new HomePage();
  const tagPage = new TagPage();

  beforeEach(() => {
    ApiHandler.initInterceptors();
  });

  it("From the landing page, clicking on a popular tag navigates the user to the results page of that tag", () => {
    homePage.visitHomePage();
    homePage.selectRandomTag();
    tagPage.isTagPageUrlCorrect();
    tagPage.doArticlesHaveTheSelectedTag();
  });
});
