import MenuNavigation from "../support/pageObjects/MenuNavigation";
import HomePage from "../support/pageObjects/HomePage";
import RegistrationPage from "../support/pageObjects/RegistrationPage";
import ApiHandler from "../support/api/ApiHandler";

describe("Real World App - Registration Page test scenarios", () => {
  let users;
  const homePage = new HomePage();
  const registrationPage = new RegistrationPage();
  const menuNavigation = new MenuNavigation();
  beforeEach(() => {
    ApiHandler.initInterceptors();
    cy.fixture("UserCredentials").then((userCredentials) => {
      users = userCredentials;
    });
  });
  it("Check whether signUp button is disabled when the fields are empty", () => {
    homePage.visitHomePage();
    menuNavigation.visitSignUpPage();
    registrationPage.isSignUpButtonDisabled();
  });
  it("Check whether signUp button is disabled when the username is filled but other fields is empty", () => {
    homePage.visitHomePage();
    menuNavigation.visitSignUpPage();
    registrationPage.fillUserName("test");
    registrationPage.isSignUpButtonDisabled();
  });
  it("Check whether signUp button is disabled when the email is filled but other fields is empty", () => {
    homePage.visitHomePage();
    menuNavigation.visitSignUpPage();
    registrationPage.fillEmail("test@example.com");
    registrationPage.isSignUpButtonDisabled();
  });
  it("Check whether signUp button is disabled when the password is filled but other fields is empty", () => {
    homePage.visitHomePage();
    menuNavigation.visitSignUpPage();
    registrationPage.fillPassword("test");
    registrationPage.isSignUpButtonDisabled();
  });
  it("Check whether password field has hidden characters", () => {
    homePage.visitHomePage();
    menuNavigation.visitSignUpPage();
    registrationPage.fillPassword("test");
    registrationPage.isPasswordHidden();
  });
  it("User able to login with correct credentials", () => {
    homePage.visitHomePage();
    menuNavigation.visitSignUpPage();
    registrationPage.isSignUpPageUrlCorrect();
    registrationPage.signUpUser(
      users.correctUser.username,
      users.correctUser.email,
      users.correctUser.password,
      200,
      "SuccessfulSignUpResponse"
    );
    registrationPage.isSignUpSuccessful();
    registrationPage.isUserNameDisplayedOnTheTopRight();
  });
  it("User should not able to login with incorrect credentials", () => {
    homePage.visitHomePage();
    menuNavigation.visitSignUpPage();
    registrationPage.isSignUpPageUrlCorrect();
    registrationPage.signUpUser(
      users.incorrectUser.username,
      users.incorrectUser.email,
      users.incorrectUser.password,
      422,
      "UnsuccessfulSignUpResponse"
    );
    registrationPage.isSignUpUnsuccessful();
    registrationPage.isUserNameAbsentOnTheTopRight();
  });
  it("Error messages shown in case of unauthorized login", () => {
    homePage.visitHomePage();
    menuNavigation.visitSignUpPage();
    registrationPage.isSignUpPageUrlCorrect();
    registrationPage.signUpUser(
      users.incorrectUser.username,
      users.incorrectUser.email,
      users.incorrectUser.password,
      422,
      "UnsuccessfulSignUpResponse"
    );
    registrationPage.areErrorMessagesShown();
  });
});
