const allArticlesEndPoint = "https://api.realworld.io/api/articles*";
const articleEndPoint = "https://api.realworld.io/api/articles/*";
const profileEndPoint = "https://api.realworld.io/api/profiles/*";
const tagsEndPoint = "https://api.realworld.io/api/tags";
const loginUserEndPoint = "https://api.realworld.io/api/users/login";

class ApiHandler {
  static initInterceptors() {
    cy.intercept({
      method: "GET",
      url: allArticlesEndPoint,
    }).as("allArticles");
    cy.intercept({
      method: "GET",
      url: articleEndPoint,
    }).as("article");
    cy.intercept({
      method: "GET",
      url: profileEndPoint,
    }).as("profile");
    cy.intercept({
      method: "GET",
      url: tagsEndPoint,
    }).as("tags");
    cy.intercept({
      method: "POST",
      url: loginUserEndPoint,
    }).as("loginUser");
  }
}

export default ApiHandler;
