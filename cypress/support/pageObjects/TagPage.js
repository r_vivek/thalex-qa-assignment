const tagPageUrl = "tag/";
const tagListSelector = "[data-qa='tag-list']";

class TagPage {
  //Verify whether URL is correct
  isTagPageUrlCorrect() {
    cy.get("@selectedTag").then((selectedTag) => {
      cy.url().should("contain", tagPageUrl + selectedTag);
    });
  }

  //Verify whether the articles displayed have the selected tag
  doArticlesHaveTheSelectedTag() {
    cy.get("@selectedTag").then((selectedTag) => {
      cy.get(tagListSelector).then((tagList) => {
        tagList.each((index, element) => {
          cy.wrap(element).should("be.visible");
          expect(element).to.contain(selectedTag);
        });
      });
    });
  }
}

export default TagPage;
