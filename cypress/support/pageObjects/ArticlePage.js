const articlePageUrl = "article/";
const articleHeaderSelector = "[data-qa='article-header']";

class ArticlePage {
  //Verify whether URL is correct
  IsArticlePageUrlCorrect() {
    cy.get("@selectedArticle").then((selectedArticle) => {
      cy.url().should("contain", articlePageUrl + selectedArticle.slug);
    });
  }

  //Check whether the article header is same as the selected article title
  IsArticleHeaderCorrect() {
    cy.get("@article").then((articleXhr) => {
      const articleTitle = articleXhr.response.body.article.title;
      cy.get(articleHeaderSelector)
        .should("be.visible")
        .should("have.text", articleTitle);
    });
  }
}

export default ArticlePage;
