const loginPageUrl = "login";
const emailFieldSelector = "[data-qa='email']";
const passwordFieldSelector = "[data-qa='password']";
const loginButtonSelector = "[data-qa='login-button']";
const profileLinkSelector = "[data-qa='profile'] a";
const errorMessagesSelector = "[data-qa='error-messages']";

class LoginPage {
  isLoginPageUrlCorrect() {
    cy.url().should("contain", loginPageUrl);
  }

  isLoginButtonDisabled() {
    cy.get(loginButtonSelector).should("be.disabled");
  }

  fillEmail(email) {
    cy.get(emailFieldSelector).type(email);
  }

  fillPassword(password) {
    cy.get(passwordFieldSelector).type(password);
  }

  isPasswordHidden() {
    cy.get(passwordFieldSelector)
      .invoke("attr", "type")
      .should("eq", "password");
  }

  loginUser(user) {
    cy.login(user.email, user.password);
    cy.wait("@loginUser");
  }

  isLoginSuccessful() {
    cy.get("@loginUser").then((loginUserXhr) => {
      expect(loginUserXhr.response.statusCode).to.be.eq(200);
    });
  }

  isUserNameDisplayedOnTheTopRight() {
    cy.get("@loginUser").then((loginUserXhr) => {
      const userName = loginUserXhr.response.body.user.username;
      cy.get(profileLinkSelector)
        .should("be.visible")
        .should("include.text", userName);
    });
  }

  isLoginUnauthorized() {
    cy.get("@loginUser").then((loginUserXhr) => {
      expect(loginUserXhr.response.statusCode).to.be.eq(401);
    });
  }

  isUserNameAbsentOnTheTopRight() {
    cy.get(profileLinkSelector).should("not.exist");
  }

  areErrorMessagesShown() {
    cy.get("@loginUser").then((loginUserXhr) => {
      const errorObject = loginUserXhr.response.body.errors;
      let errors = [];
      Object.keys(errorObject).forEach((key) => {
        errors.push(`${key} ${JSON.stringify(errorObject[key])}`);
      });
      cy.get(errorMessagesSelector).then((errorMessages) => {
        errorMessages.each((index, element) => {
          cy.wrap(element).should("be.visible");
          expect(element.innerText.replaceAll(" ", "")).to.include(
            errors[index].replaceAll(" ", "")
          );
        });
      });
    });
  }
}

export default LoginPage;
