const signUpPageUrl = "register";
const userNameFieldSelector = "[data-qa='username']";
const emailFieldSelector = "[data-qa='email']";
const passwordFieldSelector = "[data-qa='password']";
const signUpButtonSelector = "[data-qa='signUp-button']";
const profileLinkSelector = "[data-qa='profile'] a";
const errorMessagesSelector = "[data-qa='error-messages'] li";
const usersEndPoint = "https://api.realworld.io/api/users";

class RegistrationPage {
  isSignUpPageUrlCorrect() {
    cy.url().should("contain", signUpPageUrl);
  }

  isSignUpButtonDisabled() {
    cy.get(signUpButtonSelector).should("be.disabled");
  }

  fillUserName(username) {
    cy.get(userNameFieldSelector).type(username);
  }

  fillEmail(email) {
    cy.get(emailFieldSelector).type(email);
  }

  fillPassword(password) {
    cy.get(passwordFieldSelector).type(password);
  }

  isPasswordHidden() {
    cy.get(passwordFieldSelector)
      .invoke("attr", "type")
      .should("eq", "password");
  }

  signUpUser(username, email, password, responseStatusCode, responseFixture) {
    this.fillUserName(username);
    this.fillEmail(email);
    this.fillPassword(password);
    cy.fixture(responseFixture)
      .then((responseFixture) => {
        cy.intercept(
          {
            method: "POST",
            url: usersEndPoint,
          },
          (req) => {
            req.reply((res) => {
              res.send(responseStatusCode, responseFixture);
            });
          }
        );
      })
      .as("signUpUser");
    cy.get(signUpButtonSelector).click();
    cy.wait("@signUpUser");
  }

  isSignUpSuccessful() {
    cy.get("@signUpUser").then((signUpUserXhr) => {
      expect(signUpUserXhr.response.statusCode).to.be.eq(200);
    });
  }

  isUserNameDisplayedOnTheTopRight() {
    cy.get("@signUpUser").then((signUpUserXhr) => {
      const userName = signUpUserXhr.response.body.user.username;
      cy.get(profileLinkSelector)
        .should("be.visible")
        .should("include.text", userName);
    });
  }

  isSignUpUnsuccessful() {
    cy.get("@signUpUser").then((signUpUserXhr) => {
      expect(signUpUserXhr.response.statusCode).to.be.eq(422);
    });
  }

  isUserNameAbsentOnTheTopRight() {
    cy.get(profileLinkSelector).should("not.exist");
  }

  areErrorMessagesShown() {
    cy.get("@signUpUser").then((signUpUserXhr) => {
      const errorObject = signUpUserXhr.response.body.errors;
      let errors = [];
      Object.keys(errorObject).forEach((key) => {
        errors.push(`${key} ${JSON.stringify(errorObject[key])}`);
      });
      cy.get(errorMessagesSelector).then((errorMessages) => {
        errorMessages.each((index, element) => {
          cy.wrap(element).should("be.visible");
          expect(element.innerText.replaceAll(" ", "")).to.include(
            errors[index].replaceAll(" ", "")
          );
        });
      });
    });
  }
}

export default RegistrationPage;
