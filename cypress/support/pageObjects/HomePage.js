const homePageUrl = "#/";
const articleSelector = "[data-qa='article']";
const articleHeaderSelector = "[data-qa='article'] h1";
const profileSelector = "[data-qa='profile']";
const tagSelector = "[data-qa='tag']";

class HomePage {
  //Visit the HomePage and wait for services to load
  visitHomePage() {
    cy.visit(homePageUrl);
    cy.wait("@allArticles");
    cy.wait("@tags");
  }

  //Verify whether URL is correct
  isHomePageUrlCorrect() {
    cy.url().should("eq", Cypress.config().baseUrl + homePageUrl);
  }

  //Check whether articles are present
  areArticlesPresent() {
    cy.get(articleSelector).should("be.visible");
  }

  //select a random article and store the object article as an alias
  selectRandomArticle() {
    cy.get("@allArticles").then((allArticlesXhr) => {
      const articles = allArticlesXhr.response.body.articles;
      cy.log(`Number of articles -  ${articles.length}`);
      cy.getRandomInt(articles.length).then((articleNumber) => {
        const selectedArticle = articles[articleNumber];
        cy.get(articleHeaderSelector)
          .eq(articleNumber)
          .should("have.text", selectedArticle.title);
        cy.log(`Selecting article number -  ${articleNumber + 1}`);
        cy.get(articleSelector).eq(articleNumber).click();
        cy.log(`Selected article object - ${JSON.stringify(selectedArticle)}`);
        cy.wrap(selectedArticle).as("selectedArticle");
      });
    });
    cy.wait("@article");
  }

  //select a random author in the list of articles and store the object author as an alias
  selectRandomUser() {
    cy.get("@allArticles").then((allArticlesXhr) => {
      let authors = [];
      allArticlesXhr.response.body.articles.forEach((article) => {
        authors.push(article.author);
      });
      cy.log(`Number of authors -  ${authors.length}`);
      cy.getRandomInt(authors.length).then((authorNumber) => {
        const selectedAuthor = authors[authorNumber];
        cy.get(profileSelector)
          .eq(authorNumber)
          .should("have.text", selectedAuthor.username);
        cy.log(`Selecting author number -  ${authorNumber + 1}`);
        cy.get(profileSelector).eq(authorNumber).click();
        cy.log(`Selected author object - ${JSON.stringify(selectedAuthor)}`);
        cy.wrap(selectedAuthor).as("selectedAuthor");
      });
    });
    cy.wait("@profile");
  }

  //select a random tag from the list and store the tag name as an alias
  selectRandomTag() {
    cy.get("@tags").then((tagsXhr) => {
      const tags = tagsXhr.response.body.tags;
      cy.log(`Number of tags -  ${tags.length}`);
      cy.getRandomInt(tags.length).then((tagNumber) => {
        const selectedTag = tags[tagNumber];
        cy.get(tagSelector).eq(tagNumber).should("have.text", selectedTag);
        cy.log(`Selecting tag number -  ${tagNumber + 1}`);
        cy.get(tagSelector).eq(tagNumber).click();
        cy.log(`Selected tag - ${JSON.stringify(selectedTag)}`);
        cy.wrap(selectedTag).as("selectedTag");
      });
    });
    cy.wait("@allArticles");
  }
}

export default HomePage;
