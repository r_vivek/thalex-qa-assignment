const profilePageUrl = "profile/";
const profileSelector = "[data-qa='profile']";
const profileHeaderSelector = "[data-qa='profile-header']";

class ProfilePage {
  //Verify whether URL is correct
  isProfilePageUrlCorrect() {
    cy.get("@selectedAuthor").then((selectedAuthor) => {
      cy.url().should("contain", profilePageUrl + selectedAuthor.username);
    });
  }

  //Check whether the articles displayed has the same author name
  areArticleAuthorsCorrect() {
    cy.get("@selectedAuthor").then((selectedAuthor) => {
      cy.get(profileSelector).then((profiles) => {
        profiles.each((index, element) => {
          expect(element.innerText).to.be.eq(selectedAuthor.username);
        });
      });
    });
  }

  //Check whether the profile header name is same as selected user
  isProfileHeaderCorrect() {
    cy.get("@profile").then((profileXhr) => {
      const profileUserName = profileXhr.response.body.profile.username;
      cy.get(profileHeaderSelector)
        .should("be.visible")
        .should("have.text", profileUserName);
    });
  }
}

export default ProfilePage;
