const loginLinkSelector = "[data-qa='login']";
const singUpLinkSelector = "[data-qa='register']";

class MenuNavigation {
  visitLoginPage() {
    cy.get(loginLinkSelector).click();
  }

  visitSignUpPage() {
    cy.get(singUpLinkSelector).click();
  }
}

export default MenuNavigation;
