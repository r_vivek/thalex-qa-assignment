// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//
// -- This is a parent command --
// Cypress.Commands.add('login', (email, password) => { ... })
//
//
// -- This is a child command --
// Cypress.Commands.add('drag', { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add('dismiss', { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This will overwrite an existing command --
// Cypress.Commands.overwrite('visit', (originalFn, url, options) => { ... })

const emailFieldSelector = "[data-qa='email']";
const passwordFieldSelector = "[data-qa='password']";
const loginButtonSelector = "[data-qa='login-button']";

Cypress.Commands.add("login", (email, password) => {
  cy.get(emailFieldSelector).type(email);
  cy.get(passwordFieldSelector).type(password);
  cy.get(loginButtonSelector).click();
});

Cypress.Commands.add("getRandomInt", (max) => {
  return Math.floor(Math.random() * max);
});
